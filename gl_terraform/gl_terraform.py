#!/usr/bin/env python3

import ci_fairy
import click
import gitlab
import json
import os
import re
import shutil
import subprocess
import tempfile
from pathlib import Path

gl = None

terraform_state = None


@click.group()
def gl_terraform():
    pass


def invalid_cache():
    """
    we don't need to re-open the terraform state file for every
    read.
    This invalids the cache to force re-opening the state file.
    """
    global terraform_state
    terraform_state = None


def get_existing_id(
    type,
    name,
    state_file="terraform.tfstate",
    stripped_state_file="stripped_terraform.tfstate",
):
    """
    check if the current terraform state has a resource
    of a given type and name
    """
    terraform = Path(state_file)
    if not terraform.exists() or os.path.getsize(terraform) == 0:
        # the current file is not usable, use the stripped version
        # if available
        stripped = Path(stripped_state_file)
        if stripped.exists():
            shutil.copyfile(stripped, terraform)
        else:
            return None

    global terraform_state

    if terraform_state is None:
        with open(terraform) as f:
            terraform_state = json.load(f)

    items = [
        e
        for e in terraform_state["resources"]
        if e["name"] == name and e["type"] == type
    ]

    if not len(items):
        return None

    return items[0]["instances"][0]["attributes"]["id"]


def terraform_import_resource(ctx, type, name, id, in_config, allow_missing=False):
    """
    import a resource if it is not already part of the
    current terraform state
    """

    exists = get_existing_id(type, name) is not None

    if not exists:
        # special case for importing a label:
        # if a label is not known to the terraform definition, the import
        # function failed at importing it. So we manually add it to a
        # definition file that we remove after
        with tempfile.NamedTemporaryFile(mode="w", suffix=".tf", dir=os.getcwd()) as fp:
            if type == "gitlab_group_label" and not in_config:
                fp.write(
                    f"""
resource "{type}" "{name}" {{
    name = "{name}"

}}
"""
                )
                fp.flush()

            args = ["terraform", "import"]
            args.extend([f"{type}.{name}", id])
            subprocess.run(args)
    elif not allow_missing:
        print(f"skipping import of {type}.{name}")


def slugify(name):
    return (
        name.replace(":", "_")
        .replace("/", "_")
        .replace("*", "star")
        .replace(" ", "_")
        .replace(".", "_")
    )


def get_current_resources(type, gl_parent):
    terraform = Path("terraform.tfstate")
    with open(terraform) as f:
        terraform_state = json.load(f)

    ids = {
        "gitlab_branch_protection": "project",
        "gitlab_group_label": "group",
        "gitlab_label": "project",
        "gitlab_project": "id",
        "gitlab_project_approval_rule": "project",
        "gitlab_project_hook": "project",
        "gitlab_project_mirror": "project",
        "gitlab_tag_protection": "project",
    }

    gitlab_id = ids[type]

    # retrieve the currently terraform-managed objects attached
    # to this object
    current_objects = dict(
        [
            (terraform_obj["name"], terraform_obj)
            for terraform_obj in terraform_state["resources"]
            if terraform_obj["type"] == type
            and int(terraform_obj["instances"][0]["attributes"][gitlab_id])
            == int(gl_parent.id)
        ]
    )

    return terraform_state, current_objects


def __import_resources(
    ctx,
    attribute,
    name_attribute,
    parent_name,
    gl_parent,
    type,
    obj,
    gl_objs,
    numbered=False,
    ignore_regexes=None,
):
    """
    import the following resource ``obj`` of terraform type ``type``:
    parent_name:
      id: obj_id
      attribute:
        name_atribute: name

    ``gl_objs`` is the pendant python-gitlab object

    if ``numbered`` is True, the resource will be numbered
    """
    try:
        current_attrs = {o.name for o in gl_objs}
    except AttributeError:
        current_attrs = {getattr(o, name_attribute) for o in gl_objs}

    obj_id = gl_parent.id

    # retrieve the currently terraform-managed objects attached
    # to this object
    terraform_state, current_objects = get_current_resources(type, gl_parent)

    # first import in our config all declared objects
    local_id = 0
    for o in obj.get(attribute, []):
        o_name = None
        local_name = None
        if not numbered:
            # the name is known beforehand
            o_name = o[name_attribute]
            if o_name not in current_attrs:
                print(o_name, f"not in current {type}")
                continue
            local_name = slugify(o_name)
        else:
            local_name = local_id
            # find out the name (id) in the current gitlab object
            for item in gl_objs:
                if getattr(item, name_attribute) == o[name_attribute]:
                    o_name = item.id
                    break
        if o_name is not None:
            terraform_name = f"{parent_name}_{local_name}"
            terraform_import_resource(
                ctx, type, terraform_name, f"{obj_id}:{o_name}", in_config=True
            )
            if terraform_name in current_objects:
                del current_objects[terraform_name]
        local_id += 1

    invalid_cache()

    # then import the ones on the server, but not in the config

    # special case here: we might have the rule with a local id
    terraform = Path("terraform.tfstate")
    with open(terraform) as f:
        terraform_state = json.load(f)

    for o in gl_objs:
        o_name = None
        local_name = None
        if not numbered:
            o_name = o.name
            local_name = slugify(o.name)
        else:
            items = [
                e
                for e in terraform_state["resources"]
                if e["type"] == type
                and e["instances"][0]["attributes"][name_attribute]
                == getattr(o, name_attribute)
            ]
            if not items:
                o_name = o.id
                local_name = o.id

        # check if that resource is actually ignored
        if o_name is not None and ignore_regexes is not None:
            for r in ignore_regexes:
                match = re.match(r, o_name)
                if match:
                    o_name = None
                    break

        if o_name is not None:
            terraform_name = f"{parent_name}_{local_name}"
            terraform_import_resource(
                ctx,
                type,
                terraform_name,
                f"{obj_id}:{o_name}",
                in_config=False,
                allow_missing=True,
            )
            if terraform_name in current_objects:
                del current_objects[terraform_name]

    # We need to also manually clean up terraform state data if we are not
    # starting from scratch
    if current_objects:
        with open(terraform) as f:
            terraform_state = json.load(f)

        # if this list is not empty, that means we have objects
        # that we previously were managing, but that have been
        # removed outside of our control
        for object_name, obj in current_objects.items():
            print(f'removing "{object_name}" from the terraform state')
            terraform_state["resources"].remove(obj)

        terraform_state["serial"] += 1

        with open(terraform, "w") as f:
            json.dump(terraform_state, f, indent=4)

        invalid_cache()


def import_labels(ctx, parent_name, gl_object, type, obj, gl_labels):
    ignore_labels = None
    if "ignore_labels" in obj:
        ignore_labels = obj["ignore_labels"]
        if not isinstance(ignore_labels, list):
            ignore_labels = [ignore_labels]

    __import_resources(
        ctx,
        "labels",
        "name",
        parent_name,
        gl_object,
        type,
        obj,
        gl_labels,
        ignore_regexes=ignore_labels,
    )

    # updating labels through terraform takes *ages*
    # terraform does one GET call per label, and we currently
    # have more than 800 labels associated with our projects.
    # tweak the terraform file ourself to update it so we
    # don't need to rely on terraform to update the state.

    terraform = Path("terraform.tfstate")
    terraform_state, current_labels = get_current_resources(type, gl_object)

    changes = False

    # iterate over the labels we got from the API
    for gl_label in gl_labels:
        name_slug = slugify(gl_label.name)
        terraform_name = f"{gl_object.path}_{name_slug}"
        if terraform_name not in current_labels:
            # the label is not currently managed
            # skip it, it will be imported when requested as
            # a terraform object
            continue

        terraform_obj = current_labels[terraform_name]

        attrs = terraform_obj["instances"][0]["attributes"]
        if (
            attrs["color"] != gl_label.color
            or attrs["description"] != gl_label.description
        ):
            attrs["color"] = gl_label.color
            attrs["description"] = gl_label.description
            changes = True

        # remove the label from the list of currently known objects
        del current_labels[terraform_obj["name"]]

    if changes:
        terraform_state["serial"] += 1

    with open(terraform, "w") as f:
        json.dump(terraform_state, f, indent=4)

    invalid_cache()


def import_protected_branches(ctx, parent_name, gl_project, obj):
    gl_branches = gl_project.protectedbranches.list(all=True)

    return __import_resources(
        ctx,
        "branches",
        "branch",
        parent_name,
        gl_project,
        "gitlab_branch_protection",
        obj,
        gl_branches,
    )


def import_protected_tags(ctx, parent_name, gl_project, obj):
    gl_tags = gl_project.protectedtags.list(all=True)

    return __import_resources(
        ctx,
        "tags",
        "tag",
        parent_name,
        gl_project,
        "gitlab_tag_protection",
        obj,
        gl_tags,
    )


def import_approval_rules(ctx, parent_name, gl_project, obj):
    gl_rules = gl_project.approvalrules.list(all=True)

    return __import_resources(
        ctx,
        "approval_rules",
        "name",
        parent_name,
        gl_project,
        "gitlab_project_approval_rule",
        obj,
        gl_rules,
        numbered=True,
    )


def import_hooks(ctx, parent_name, gl_project, obj):
    gl_hooks = gl_project.hooks.list(all=True)

    return __import_resources(
        ctx,
        "hooks",
        "url",
        parent_name,
        gl_project,
        "gitlab_project_hook",
        obj,
        gl_hooks,
        numbered=True,
    )


def import_mirrors(ctx, parent_name, gl_project, obj):
    gl_mirrors = gl_project.remote_mirrors.list(all=True)

    return __import_resources(
        ctx,
        "mirrors",
        "url",
        parent_name,
        gl_project,
        "gitlab_project_mirror",
        obj,
        gl_mirrors,
        numbered=True,
    )


def finish_import_group(ctx, group_name, group_id, group):
    gl_group = gl.groups.get(group_id)

    ###########################################################################
    # group labels
    ###########################################################################

    gl_labels = gl_group.labels.list(
        all=True, include_ancestor_groups=False, only_group_labels=True
    )

    import_labels(ctx, group_name, gl_group, "gitlab_group_label", group, gl_labels)


def finish_import_project(ctx, project_name, project_id, project):
    gl_project = gl.projects.get(project_id)

    # project level mr approvals
    terraform_import_resource(
        ctx,
        "gitlab_project_level_mr_approvals",
        f"{project_name}_mr_approvals",
        project_id,
        in_config=True,
    )

    # project labels
    gl_labels = gl_project.labels.list(all=True, include_ancestor_groups=False)

    # bug??? in the API: include_ancestor_groups=False is not properly handled for some projects
    gl_labels = [label for label in gl_labels if label.is_project_label]

    import_labels(ctx, project_name, gl_project, "gitlab_label", project, gl_labels)

    import_protected_branches(ctx, project_name, gl_project, project)

    import_protected_tags(ctx, project_name, gl_project, project)

    import_approval_rules(ctx, project_name, gl_project, project)

    import_hooks(ctx, project_name, gl_project, project)

    import_mirrors(ctx, project_name, gl_project, project)

    # the API doesn't export the current import_url
    # Overwrite the saved data with the one we have in the config.

    terraform_state, current_project = get_current_resources(
        "gitlab_project", gl_project
    )

    current_attrs = current_project[project_name]["instances"][0]["attributes"]

    if current_attrs["import_url"] is None and "import_url" in project["terraform"]:

        print("overwriting import_url in terraform state")
        current_attrs["import_url"] = project["terraform"]["import_url"]

        terraform_state["serial"] += 1

        terraform = Path("terraform.tfstate")
        with open(terraform, "w") as f:
            json.dump(terraform_state, f, indent=4)

        invalid_cache()


def import_resource(ctx, name, obj):
    type = obj["type"]
    terraform_import_resource(ctx, type, name, obj["import_id"], in_config=True)

    invalid_cache()

    id = get_existing_id(type, name)

    if type == "gitlab_group":
        finish_import_group(ctx, name, id, obj)
    elif type == "gitlab_project":
        finish_import_project(ctx, name, id, obj)


def _strip_tfstate(source, destination):
    """
    create a copy of the terraform state with data we know is safe.
    This will speed up initial deployment as we don't need to retrieve all
    data during the import phase.
    """

    with open(source) as src:
        terraform_state = json.load(src)

    # the following types do not contain sensitive data
    safe_types = (
        "gitlab_group_label",
        "gitlab_label",
    )

    terraform_state["resources"] = [
        r for r in terraform_state["resources"] if r["type"] in safe_types
    ]

    # the serial is incremented after each import, but we don't really
    # need it actually
    terraform_state["serial"] = 1

    with open(destination, "w") as dest:
        json.dump(terraform_state, dest, indent=4)


def call_ci_fairy_generate_template(ctx, config, template, output_file):
    ctx.invoke(
        ci_fairy.generate_template,
        config=[config],
        template=template,
        output_file=output_file,
    )


def generate_terraform(ctx, config, output):
    script_path = Path(os.path.realpath(__file__))

    src_path = script_path.parent
    data_path = src_path / "data"

    # rebuild the gitlab.tf file
    call_ci_fairy_generate_template(ctx, config, data_path / "gitlab.tf.tmpl", output)


@gl_terraform.command()
@click.option(
    "--output",
    help='path to the generated terraform file (default to "gitlab.tf")',
    default="gitlab.tf",
)
@click.argument("config", type=click.Path(exists=True))
@click.pass_context
def generate(ctx, config, output):
    generate_terraform(ctx, config, output)


@gl_terraform.command()
@click.option("--source", help="json input file", required=True)
@click.option("--destination", help="json stripped output file", required=True)
@click.pass_context
def strip_tfstate(ctx, source, destination):
    """
    create a copy of the terraform state with data we know is safe.
    This will speed up initial deployment as we don't need to retrieve all
    data during the import phase.
    """
    _strip_tfstate(source, destination)


@gl_terraform.command(name="import")
@click.option(
    "--output",
    help='path to the generated terraform file (default to "gitlab.tf")',
    default="gitlab.tf",
)
@click.option(
    "--server",
    help='gitlab instance URL (default to "https://gitlab.com")',
    default="https://gitlab.com",
)
@click.argument("config", type=click.Path(exists=True))
@click.pass_context
def do_import(ctx, config, server, output):
    global gl

    generate_terraform(ctx, config, output)

    gl = gitlab.Gitlab(server, private_token=os.getenv("GITLAB_TOKEN"), per_page=100)

    print("authenticating with gitlab...")
    gl.auth()
    print("done")

    data = ci_fairy.ExtendedYaml.load_from_file(config)

    for name, obj in data.items():
        if name.startswith("."):
            continue

        import_resource(ctx, name, obj)

    _strip_tfstate(source="terraform.tfstate", destination="stripped_terraform.tfstate")


@gl_terraform.command(name="plan2md")
@click.argument("plan", type=click.Path(exists=True))
@click.pass_context
def do_plan2md(ctx, plan):
    with open(plan) as f:
        plan = json.load(f)

    if "resource_changes" not in plan:
        return

    data = [
        obj
        for obj in plan["resource_changes"]
        if obj["change"]["actions"][0] != "no-op"
    ]

    if not data:
        print(
            """### No changes. Infrastructure is up-to-date.

This means that Terraform did not detect any differences between your
configuration and real physical resources that exist. As a result, no
actions need to be performed.
"""
        )
        return

    print("The following changes need to be performed:\n")

    for obj in data:
        change = obj["change"]
        action = change["actions"][0]

        print(f'### **{obj["address"]}** will be {action}d')
        print(
            f"""
```diff
 resource "{obj["type"]}" "{obj["name"]}" {{"""
        )

        def diff(before, after, indent=4, terminate=True):
            ref = before

            if before is None:
                before = {}
                ref = after
            elif after is None:
                after = {}

            # compute the biggest key lenght
            length = 0
            for key in ref.keys():
                if len(key) > length:
                    length = len(key)

            for key in ref.keys():
                prev_value = before.get(key)
                next_value = after.get(key)
                padding = " " * (length - len(key) + 1)
                indent_chars = " " * indent

                if list in [type(prev_value), type(next_value)]:
                    print(f" {indent_chars}{key}{padding} = [")

                    # ensure we have lists in both hands
                    if prev_value is None:
                        prev_value = []
                    if next_value is None:
                        next_value = []

                    if len(prev_value) == len(next_value) == 1 and isinstance(
                        prev_value[0], dict
                    ):
                        # special case, just diff between the 2 values
                        print(f" {indent_chars}    {{")
                        diff(prev_value[0], next_value[0], indent + 8, terminate=False)
                        print(f' {" "*indent}    }}', end="")

                    else:
                        sep = None

                        # first check existing items and removed ones
                        for i in range(len(prev_value)):
                            if sep is not None:
                                print(sep)
                            sep = ","

                            if prev_value[i] in next_value:
                                if isinstance(prev_value[i], dict):
                                    # we have a terraform object
                                    print(f" {indent_chars}    {{")
                                    diff(
                                        prev_value[i],
                                        next_value[i],
                                        indent + 8,
                                        terminate=False,
                                    )
                                    print(f' {" "*indent}    }}', end="")
                                else:
                                    print(f' {" "*indent}    ...', end="")
                            else:
                                if isinstance(prev_value[i], dict):
                                    # we have a terraform object
                                    print(f" {indent_chars}    {{")
                                    diff(
                                        prev_value[i], None, indent + 8, terminate=False
                                    )
                                    print(f' {" "*indent}    }}', end="")
                                else:
                                    print(f'-{" "*indent}    {prev_value[i]}', end="")

                        # now check for any added items
                        for i in range(len(next_value)):
                            if sep is not None:
                                print(sep)
                            sep = ","

                            if next_value[i] in prev_value:
                                # we already marked it in the previous pass
                                pass
                            else:
                                if isinstance(next_value[i], dict):
                                    # we have a terraform object
                                    print(f" {indent_chars}    {{")
                                    diff(
                                        None, next_value[i], indent + 8, terminate=False
                                    )
                                    print(f' {" "*indent}    }}', end="")
                                else:
                                    print(f'+{" "*indent}    {next_value[i]}', end="")

                    print(f'\n {" "*indent}]')

                    continue

                if prev_value == next_value:
                    print(f" {indent_chars}{key}{padding} = ...")
                    continue

                if prev_value is not None:
                    print(f"-{indent_chars}{key}{padding} = {prev_value}")
                if next_value is not None:
                    print(f"+{indent_chars}{key}{padding} = {next_value}")

            if terminate:
                print(f' {" "*(indent - 4)}}}')

        before = change["before"]
        after = change["after"]

        diff(before, after)

        print(
            """```
"""
        )


def main(*args, **kwargs):
    gl_terraform(*args, **kwargs)


if __name__ == "__main__":
    main()
