#!/bin/bash

set -e
set -x

TF_DIR="${TF_DIR:-example}"

DIRNAME=$(basename $PWD)

# ttl for the provided container
STEPS=300
KEEP_ALIVE_FILE=/app/$DIRNAME/${TF_DIR}/terraform.tfstate


IS_RUNNING=$(podman ps --filter name=gitlab-terraform --quiet)

# if the container is not running start it
# it will check for updates on /app/terraform.tfstate every $
if [[ ! $IS_RUNNING ]]
then
  podman run --rm --detach --name gitlab-terraform \
             registry.gitlab.com/btissoir/gitlab-terraform/alpine/latest:2022-09-29.1 \
             bash -c "mkdir -p $(dirname $KEEP_ALIVE_FILE); \
                      touch $KEEP_ALIVE_FILE; \
                      while : ; do \
                          sleep $STEPS ; \
                          KEEP_ALIVE=\$(stat -c %X $KEEP_ALIVE_FILE) ; \
                          CURRENT_TIME=\$(date "+%s") ; \
                          echo \$KEEP_ALIVE \$CURRENT_TIME ; \
                          (( \$(( \$KEEP_ALIVE + $STEPS )) > \$CURRENT_TIME )) || break ; \
                      done"
fi

# touch the keep_alive file to give us $STEPS time to copy files to the container
podman exec gitlab-terraform touch $KEEP_ALIVE_FILE

podman cp $PWD gitlab-terraform:/app
podman exec -ti gitlab-terraform bash -c "cd /app/$DIRNAME; pip install --user ."

# run terraform init when the container has just been spawned
if [[ ! $IS_RUNNING ]]
then
  podman exec -ti gitlab-terraform bash -c "cd /app/$DIRNAME/${TF_DIR}; terraform init"
fi

podman exec --env GITLAB_TOKEN=$GITLAB_TOKEN \
            -ti gitlab-terraform bash -c "export PATH=~/.local/bin/:$PATH; \
                                          cd /app/$DIRNAME/${TF_DIR}; \
                                          $@"

mkdir -p ${TF_DIR}
podman cp gitlab-terraform:/app/$DIRNAME/${TF_DIR}/gitlab.tf ${TF_DIR}/
podman cp gitlab-terraform:/app/$DIRNAME/.gitlab-ci.yml .
podman cp gitlab-terraform:/app/$DIRNAME/make_in_container.sh .
podman cp gitlab-terraform:/app/$DIRNAME/${TF_DIR}/terraform.tfstate ${TF_DIR}/
podman cp gitlab-terraform:/app/$DIRNAME/${TF_DIR}/stripped_terraform.tfstate ${TF_DIR}/