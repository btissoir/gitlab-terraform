#!/bin/bash

set -e
set -x

# download and install terraform
TERRAFORM_VERSION=1.3.1

wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip
mv terraform /usr/local/bin
rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# install various pip dependencies
pip install gitlab
