#!/usr/bin/env python3

from setuptools import setup

setup(name='gl-terraform',
      version='0.1',
      description='Commandline tools for maintaining gitlab projects through terraform',
      url='http://gitlab.com/btissoir/gitlab-terraform',
      author='The fdo collective',
      author_email='check.the@git.history',
      license='MIT',
      package_dir={'': 'gl_terraform'},
      packages=[''],
      py_modules=['terraform_import'],
      entry_points={
          'console_scripts': [
              'gl-terraform = gl_terraform:main',
          ]
      },
      package_data={
          '': ['data/*.tmpl'],
      },
      classifiers=[
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6'
      ],
      python_requires='>=3.6',
      include_package_data=True,
      install_requires=[
          'ci-fairy',
          'python-gitlab',
          'click',
          'pyyaml',
          'jinja2',
      ],
      )
